﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestfulWebService.Models
{
    public class Model_Word
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _text;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public Model_Word()
        {
            Id = "";
            Text = "";
        }
    }
}