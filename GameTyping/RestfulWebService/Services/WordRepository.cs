﻿using System;
using RestfulWebService.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestfulWebService.Services;

namespace RestfulWebService.Services
{
    public class WordRepository
    {
        public DB_GameTyping_MappingDataContext db;
        //private const string CacheKey = "UserStore";

        public WordRepository()
        {
            db = new DB_GameTyping_MappingDataContext();
            
            #region
            /*
            var ctx = HttpContext.Current;
            if (ctx != null)
            {
                if (ctx.Cache[CacheKey] == null)
                {
                    var db_select = (from u in db.Users
                                     select u).ToList();
                    Model_User[] userList = new Model_User[db_select.Count];
                    for (int i = 0; i < db_select.Count; i++)
                    {
                        userList[i] = new Model_User();
                        userList[i].Id_user = db_select[i].ID;
                        userList[i].Password_user = db_select[i].PASSWORD;
                    }
                    ctx.Cache[CacheKey] = userList;
                }
            } */
            #endregion
        }

        public Model_Word[] GetAllWord()
        {
            #region
            /*
            var ctx = HttpContext.Current;
            if (ctx != null)
            {
                return (Model_User[])ctx.Cache[CacheKey];
            } */
            #endregion

            var db_select = (from u in db.WORDs
                             select u).ToList();

            Model_Word[] list = new Model_Word[db_select.Count];
            for (int i = 0; i < db_select.Count; i++)
            {
                list[i] = new Model_Word();
                list[i].Id = db_select[i].ID;
                list[i].Text = db_select[i].TEXT;
            }

            return list;
        }

        public Model_Word GetWordByID(string id)
        {
            if (id.CompareTo("-1") == 0)
            {             
                var db_select = (from u in db.WORDs
                                 select u).ToList();

                int index = db_select.Count();
                index = new Random().Next(index);

                return new Model_Word() { Id = db_select[index].ID, Text = db_select[index].TEXT };
            }

            if (CheckExistWord(id))
            {
                WORD w = db.WORDs.Single(c => c.ID == id);
                Model_Word mw = new Model_Word() { Id = w.ID, Text = w.TEXT };
                return mw;
            }

            return null;
        }

        public bool CheckExistWord(string id)
        {
            var db_select = (from u in db.WORDs
                             where u.ID == id
                             select u).ToList();
            if (db_select.Count > 0)
                return true;
            else
                return false;
        }

        public bool InsertWord(Model_Word u)
        {
            if (!CheckExistWord(u.Id))
            {
                WORD w = new WORD();
                w.ID = u.Id;
                w.TEXT = u.Text;

                db.WORDs.InsertOnSubmit(w);
                db.SubmitChanges();
                return true;
            }
            else
                return false;
        }

        public Model_Word UpdateWord(Model_Word u)
        {
            if (CheckExistWord(u.Id))
            {
                WORD w = db.WORDs.Single(c => c.ID == u.Id);
                w.TEXT = u.Text;

                db.SubmitChanges();
            }
            return u;
        }

        public bool DeleteWord(string id)
        {  
            if (CheckExistWord(id))
            {
                WORD w = db.WORDs.Single(c => c.ID == id);
                db.WORDs.DeleteOnSubmit(w);
                db.SubmitChanges();
                return true;
            }
            return false;
        }       
    }
}