﻿using RestfulWebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestfulWebService.Services
{
    public class ScoreRepository
    {
        public DB_GameTyping_MappingDataContext db;
        //private const string CacheKey = "ProjectStore";

        public ScoreRepository()
        {
            db = new DB_GameTyping_MappingDataContext();

            #region
            /*
            var ctx = HttpContext.Current;
            if (ctx != null)
            {
                if (ctx.Cache[CacheKey] == null)
                {
                    var db_select = (from u in db.Projects
                                     select u).ToList();
                    Model_Project[] projectList = new Model_Project[db_select.Count];
                    for (int i = 0; i < db_select.Count; i++)
                    {
                        projectList[i] = new Model_Project();
                        projectList[i].Id_project = db_select[i].ID_PROJECT;
                        projectList[i].Name_project = db_select[i].NAME_PROJECT;
                        projectList[i].Id_user = db_select[i].ID_USER;
                    }
                    ctx.Cache[CacheKey] = projectList;
                }
            } */
            #endregion
        }

        public Model_Score[] GetAllScore()
        {
            #region
            /*
            var ctx = HttpContext.Current;
            if (ctx != null)
            {
                return (Model_Project[])ctx.Cache[CacheKey];
            } */
            #endregion

            var db_select = (from u in db.SCOREs
                             select u).ToList();

            Model_Score[] list = new Model_Score[db_select.Count];
            for (int i = 0; i < db_select.Count; i++)
            {
                list[i] = new Model_Score();
                list[i].Id = db_select[i].ID;
                list[i].Name = db_select[i].NAME;
                list[i].Point = db_select[i].POINT;
                list[i].Email = db_select[i].EMAIL;
            }

            return list;
        }

        public Model_Score GetScoreByID(string id)
        {
            if (CheckExistScoreID(id))
            {
                SCORE s = db.SCOREs.Single(c => c.ID == id);
                Model_Score ms = new Model_Score()
                { 
                    Id = s.ID, 
                    Name = s.NAME,
                    Point = s.POINT,
                    Email = s.EMAIL
                };
                return ms;
            }
            return null;
        }

        public bool CheckExistScoreID(string id)
        {
            var db_select = (from u in db.SCOREs
                             where u.ID == id
                             select u).ToList();
            if (db_select.Count > 0)
                return true;
            else
                return false;
        }

        public bool InsertScore(Model_Score u)
        {
            if (u.Id.CompareTo("-1") == 0)
            {
                int count = db.SCOREs.Count();
                count++;
                u.Id = count.ToString("00000000");
            }

            if (!CheckExistScoreID(u.Id))
            {
                SCORE info = new SCORE() { 
                    ID = u.Id, 
                    NAME = u.Name,
                    POINT = u.Point,
                    EMAIL = u.Email
                };
                db.SCOREs.InsertOnSubmit(info);
                db.SubmitChanges();
                return true;
            }
            else
                return false;
        }

        public Model_Score UpdateScore(Model_Score u)
        {
            if (CheckExistScoreID(u.Id))
            {
                SCORE s = db.SCOREs.Single(c => c.ID == u.Id);
                s.NAME = u.Name;
                s.POINT = u.Point;
                s.EMAIL = u.Email;
                db.SubmitChanges();
            }
            return u;
        }

        public bool DeleteScore(string id)
        {
            if (CheckExistScoreID(id))
            {
                SCORE u = db.SCOREs.Single(c => c.ID == id);
                db.SCOREs.DeleteOnSubmit(u);
                db.SubmitChanges();
                return true;
            }
            return false;
        }       
    }
}