﻿using RestfulWebService.Models;
using RestfulWebService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace RestfulWebService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ScoreController : ApiController
    {
        public ScoreRepository scoreRepository;

        public ScoreController()
        {
            scoreRepository = new ScoreRepository();
        }

        public IEnumerable<Model_Score> Get()
        {
            return scoreRepository.GetAllScore();
        }

        public Model_Score Get(string id)
        {
            var u = scoreRepository.GetScoreByID(id);

            if (u == null)
            {
                throw new HttpResponseException(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("Score not found")
                });
            }

            return u;
        }

        public HttpResponseMessage Post(Model_Score u)
        {
            if (scoreRepository.InsertScore(u))
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, u);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = u.Id }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        public Model_Score Put(Model_Score u)
        {
            try
            {
                u = scoreRepository.UpdateScore(u);
            }
            catch (Exception)
            {
                throw new HttpResponseException(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("Score not found")
                });
            }

            return u;
        }

        public HttpResponseMessage Delete(string id)
        {
            if (scoreRepository.DeleteScore(id))
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, "Delete Success!");
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = UrlParameter.Optional }));
                return response;
            }
            else
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("Score not found")
                };
            }
        }       
    }
}
