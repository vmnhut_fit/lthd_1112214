﻿using RestfulWebService.Models;
using RestfulWebService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace RestfulWebService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WordController : ApiController
    {
        public WordRepository wordRepository;

        public WordController()
        {
            wordRepository = new WordRepository();
        }

        public IEnumerable<Model_Word> Get()
        {
            return wordRepository.GetAllWord();
        }

        public Model_Word Get(string id)
        {
            var u = wordRepository.GetWordByID(id);

            if (u == null)
            {
                throw new HttpResponseException(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("Word not found")
                });
            }

            return u;
        }

        public HttpResponseMessage Post(Model_Word u)
        {
            if (wordRepository.InsertWord(u))
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, u);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = u.Id }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        public Model_Word Put(Model_Word u)
        {
            try
            {
                u = wordRepository.UpdateWord(u);
            }
            catch (Exception)
            {
                throw new HttpResponseException(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("Word not found")
                });
            }

            return u;
        }

        public HttpResponseMessage Delete(string id)
        {
            if (wordRepository.DeleteWord(id))
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, "Delete Success!");
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = UrlParameter.Optional }));
                return response;
            }
            else
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("Word not found")
                };
            }
        }       
    }
}
