﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestfulWebService.Models
{
    public class Model_Score
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private double _point;

        public double Point
        {
            get { return _point; }
            set { _point = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public Model_Score()
        {
            Id = "";
            Name = "";
            Point = 0;
        }
    }
}